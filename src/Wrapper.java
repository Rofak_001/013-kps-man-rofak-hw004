import java.util.ArrayList;

public class Wrapper<T> {
    private ArrayList<T> arrayList=new ArrayList <>();
    public void addItem(T item)throws NumberFormatException,DuplicateException{
        if(item==null)throw new NumberFormatException("Inputted null value");
        else if(arrayList.contains(item))throw new DuplicateException("Duplicate Value: "+item);
        else {
            arrayList.add(item);
        }
    }
    public T getItem(int index){
        return arrayList.get(index);
    }
    public int size(){
        return arrayList.size();
    }
}
